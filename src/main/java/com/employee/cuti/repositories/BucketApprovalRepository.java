package com.employee.cuti.repositories;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.employee.cuti.models.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long> {
	//Read by id
	@Query(value = "Select * from bucket_approval where id_user_leave_request IN (select id_user_leave_request from user_leave_request where id_user = :userId)", nativeQuery = true)	
	Page<BucketApproval> findReqByUser(@Param("userId") Long userId, Pageable pagable);
			
}
