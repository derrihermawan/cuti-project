package com.employee.cuti.dtomodels;

import java.util.Date;

import com.employee.cuti.models.Users;


public class UserLeaveRequestDTO {
	private Long idUserLeaveRequest;
	private UsersDTO users;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private Integer remainingDaysOff;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private String statusPengajuan;
	private Date pengajuanDate;
	public UserLeaveRequestDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserLeaveRequestDTO(Long idUserLeaveRequest, UsersDTO users, Date leaveDateFrom, Date leaveDateTo,
			String description, Integer remainingDaysOff, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate, String statusPengajuan, Date pengajuanDate) {
		super();
		this.idUserLeaveRequest = idUserLeaveRequest;
		this.users = users;
		this.leaveDateFrom = leaveDateFrom;
		this.leaveDateTo = leaveDateTo;
		this.description = description;
		this.remainingDaysOff = remainingDaysOff;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.statusPengajuan = statusPengajuan;
		this.pengajuanDate = pengajuanDate;
	}

	public Long getIdUserLeaveRequest() {
		return idUserLeaveRequest;
	}

	public void setIdUserLeaveRequest(Long idUserLeaveRequest) {
		this.idUserLeaveRequest = idUserLeaveRequest;
	}

	public UsersDTO getUsers() {
		return users;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}

	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}

	public Date getLeaveDateTo() {
		return leaveDateTo;
	}

	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getRemainingDaysOff() {
		return remainingDaysOff;
	}

	public void setRemainingDaysOff(Integer remainingDaysOff) {
		this.remainingDaysOff = remainingDaysOff;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatusPengajuan() {
		return statusPengajuan;
	}

	public void setStatusPengajuan(String statusPengajuan) {
		this.statusPengajuan = statusPengajuan;
	}

	public Date getPengajuanDate() {
		return pengajuanDate;
	}

	public void setPengajuanDate(Date pengajuanDate) {
		this.pengajuanDate = pengajuanDate;
	}
	
	
	
}
