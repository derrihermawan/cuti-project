package com.employee.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.cuti.dtomodels.UsersDTO;
import com.employee.cuti.models.Users;
import com.employee.cuti.repositories.UsersRepository;

@RestController
@RequestMapping("/api/users")
public class UsersController {
	@Autowired
	UsersRepository usersRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("create")
	public Map<String, Object> createDataUser(@Valid @RequestBody UsersDTO usersDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Users users = modelMapper.map(usersDTO, Users.class);
		usersRepository.save(users);
		result.put("Status", 200);
		result.put("Message", "Create data user succesful");
		result.put("Data", users);
		return result;
	}
	@GetMapping("/readAll")
	public Map<String, Object> readAllData(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Users> listUsers = usersRepository.findAll();
		List<UsersDTO> listUsersDTO = new ArrayList<UsersDTO>();
		
		for(Users users : listUsers) {
			UsersDTO usersDTO = modelMapper.map(users, UsersDTO.class);
			listUsersDTO.add(usersDTO);
		}
		result.put("Status", 200);
		result.put("Messsage", "read all data users succesful");
		result.put("Data", listUsersDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> readDataById(@PathVariable(value = "id")Long idUsers){
		Map<String, Object> result = new HashMap<String, Object>();
		Users users = usersRepository.findById(idUsers).get();
		UsersDTO usersDTO = modelMapper.map(users, UsersDTO.class);
		result.put("Status", 200);
		result.put("Messsage", "Read data users by id succesful");
		result.put("Data", usersDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateData(@PathVariable(value = "id") Long idUsers, @Valid @RequestBody UsersDTO usersDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Users users = usersRepository.findById(idUsers).get();
		Date date = users.getCreatedDate();
		users = modelMapper.map(usersDTO, Users.class);
		users.setIdUser(idUsers);
		users.setCreatedDate(date);
		usersRepository.save(users);
		result.put("Status", 200);
		result.put("Messsage", "update data users succesful");
		result.put("Data", users);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deleteUsers(@PathVariable(value = "id") Long idUsers){
		Users users = usersRepository.findById(idUsers).get();
		usersRepository.delete(users);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Messsage", "Delete data users succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}

}
