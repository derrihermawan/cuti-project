package com.employee.cuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.cuti.models.PositionLeave;

@Repository
public interface PositionLeaveRepository extends JpaRepository<PositionLeave, Long>{

}
