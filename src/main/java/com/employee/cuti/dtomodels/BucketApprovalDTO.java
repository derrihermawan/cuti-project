package com.employee.cuti.dtomodels;

import java.util.Date;

import com.employee.cuti.models.UserLeaveRequest;
import com.employee.cuti.models.Users;


public class BucketApprovalDTO {
	private Long idBucketApproval;
	private UserLeaveRequestDTO userLeaveRequest;
	private UsersDTO users;
	private String status;
	private String resolverReason;
	private Date resolverDate;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;	
	
	public BucketApprovalDTO() {
		// TODO Auto-generated constructor stub
	}

	public BucketApprovalDTO(Long idBucketApproval, UserLeaveRequestDTO userLeaveRequest, UsersDTO users, String status,
			String resolverReason, Date resolverDate, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		super();
		this.idBucketApproval = idBucketApproval;
		this.userLeaveRequest = userLeaveRequest;
		this.users = users;
		this.status = status;
		this.resolverReason = resolverReason;
		this.resolverDate = resolverDate;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	public Long getIdBucketApproval() {
		return idBucketApproval;
	}

	public void setIdBucketApproval(Long idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}

	public UserLeaveRequestDTO getUserLeaveRequest() {
		return userLeaveRequest;
	}

	public void setUserLeaveRequest(UserLeaveRequestDTO userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

	public UsersDTO getUsers() {
		return users;
	}

	public void setUsers(UsersDTO users) {
		this.users = users;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResolverReason() {
		return resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	public Date getResolverDate() {
		return resolverDate;
	}

	public void setResolverDate(Date resolverDate) {
		this.resolverDate = resolverDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
