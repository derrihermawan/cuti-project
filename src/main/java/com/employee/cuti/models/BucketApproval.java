package com.employee.cuti.models;
// Generated Jan 14, 2020 5:36:50 PM by Hibernate Tools 5.1.10.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * BucketApproval generated by hbm2java
 */
@Entity
@Table(name = "bucket_approval", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdDate", "updatedDate"}, 
        allowGetters = true)
public class BucketApproval implements java.io.Serializable {

	private Long idBucketApproval;
	private UserLeaveRequest userLeaveRequest;
	private Users users;
	private String status;
	private String resolverReason;
	private Date resolverDate;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;

	public BucketApproval() {
	}

	public BucketApproval(Long idBucketApproval, UserLeaveRequest userLeaveRequest) {
		this.idBucketApproval = idBucketApproval;
		this.userLeaveRequest = userLeaveRequest;
	}

	public BucketApproval(Long idBucketApproval, UserLeaveRequest userLeaveRequest, Users users, String status,
			String resolverReason, Date resolverDate, String createdBy, Date createdDate, String updatedBy,
			Date updatedDate) {
		this.idBucketApproval = idBucketApproval;
		this.userLeaveRequest = userLeaveRequest;
		this.users = users;
		this.status = status;
		this.resolverReason = resolverReason;
		this.resolverDate = resolverDate;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	@Id
	@Column(name = "id_bucket_approval", unique = true, nullable = false)
	public Long getIdBucketApproval() {
		return this.idBucketApproval;
	}

	public void setIdBucketApproval(Long idBucketApproval) {
		this.idBucketApproval = idBucketApproval;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user_leave_request", nullable = false)
	public UserLeaveRequest getUserLeaveRequest() {
		return this.userLeaveRequest;
	}

	public void setUserLeaveRequest(UserLeaveRequest userLeaveRequest) {
		this.userLeaveRequest = userLeaveRequest;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "resolved_by")
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	@Column(name = "status")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "resolver_reason")
	public String getResolverReason() {
		return this.resolverReason;
	}

	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "resolver_date", length = 13)
	public Date getResolverDate() {
		return this.resolverDate;
	}

	public void setResolverDate(Date resolverDate) {
		this.resolverDate = resolverDate;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "created_date", length = 33)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@Column(name = "updated_date", length = 27)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
