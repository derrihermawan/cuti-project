package com.employee.cuti.models;
// Generated Jan 14, 2020 5:36:50 PM by Hibernate Tools 5.1.10.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * PositionLeave generated by hbm2java
 */
@Entity
@Table(name = "position_leave", schema = "public")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdDate", "updatedDate"}, 
        allowGetters = true)
public class PositionLeave implements java.io.Serializable {

	private Long idPositionLeave;
	private Position position;
	private int leaveAllowance;
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;

	public PositionLeave() {
	}

	public PositionLeave(Long idPositionLeave, Position position, int leaveAllowance) {
		this.idPositionLeave = idPositionLeave;
		this.position = position;
		this.leaveAllowance = leaveAllowance;
	}

	public PositionLeave(Long idPositionLeave, Position position, int leaveAllowance, String createdBy,
			Date createdDate, String updatedBy, Date updatedDate) {
		this.idPositionLeave = idPositionLeave;
		this.position = position;
		this.leaveAllowance = leaveAllowance;
		this.createdBy = createdBy;
		this.createdDate = createdDate;
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_pos_leave_id_position_leave_seq")
	@SequenceGenerator(name="generator_pos_leave_id_position_leave_seq", sequenceName="pos_leave_id_position_leave_seq", schema = "public", allocationSize = 1)
	@Column(name = "id_position_leave", unique = true, nullable = false)
	public Long getIdPositionLeave() {
		return this.idPositionLeave;
	}

	public void setIdPositionLeave(Long idPositionLeave) {
		this.idPositionLeave = idPositionLeave;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_position", nullable = false)
	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@Column(name = "leave_allowance", nullable = false)
	public int getLeaveAllowance() {
		return this.leaveAllowance;
	}

	public void setLeaveAllowance(int leaveAllowance) {
		this.leaveAllowance = leaveAllowance;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "created_date", length = 33)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@Column(name = "updated_date", length = 33)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
