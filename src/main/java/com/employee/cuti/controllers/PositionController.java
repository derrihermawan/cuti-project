package com.employee.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.cuti.dtomodels.PositionDTO;
import com.employee.cuti.models.Position;
import com.employee.cuti.repositories.PositionRepository;

@RestController
@RequestMapping("api/position")
public class PositionController {
	@Autowired
	PositionRepository positionRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createPosition(@Valid @RequestBody PositionDTO positionDTO ){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = modelMapper.map(positionDTO, Position.class);
		positionRepository.save(position);
		
		result.put("Status", 200);
		result.put("Messsage", "Input data position succesful");
		result.put("Data", position);
		return result;
		
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> readAllData(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Position> listPosition = positionRepository.findAll();
		List<PositionDTO> listPositionDTO = new ArrayList<PositionDTO>();
		
		for(Position position : listPosition) {
			PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
			listPositionDTO.add(positionDTO);
		}
		result.put("Status", 200);
		result.put("Messsage", "read all data position succesful");
		result.put("Data", listPositionDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> readDataById(@PathVariable(value = "id")Long idPosition){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepository.findById(idPosition).get();
		PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
		result.put("Status", 200);
		result.put("Messsage", "Read data position by id succesful");
		result.put("Data", positionDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateData(@PathVariable(value = "id") Long idPosition, @Valid @RequestBody PositionDTO positionDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		Position position = positionRepository.findById(idPosition).get();
		Date date = position.getCreatedDate();
		position = modelMapper.map(positionDTO, Position.class);
		position.setIdPosition(idPosition);
		position.setCreatedDate(date);
		positionRepository.save(position);
		result.put("Status", 200);
		result.put("Messsage", "update data position succesful");
		result.put("Data", position);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePosition(@PathVariable(value = "id") Long idPosition){
		Position position = positionRepository.findById(idPosition).get();
		positionRepository.delete(position);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Messsage", "Delete data position succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
	
}
