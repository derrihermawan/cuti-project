package com.employee.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.cuti.dtomodels.BucketApprovalDTO;
import com.employee.cuti.dtomodels.PositionDTO;
import com.employee.cuti.dtomodels.PositionLeaveDTO;
import com.employee.cuti.dtomodels.UserLeaveRequestDTO;
import com.employee.cuti.models.BucketApproval;
import com.employee.cuti.models.Position;
import com.employee.cuti.models.PositionLeave;
import com.employee.cuti.models.UserLeaveRequest;
import com.employee.cuti.models.Users;
import com.employee.cuti.repositories.BucketApprovalRepository;
import com.employee.cuti.repositories.PositionLeaveRepository;
import com.employee.cuti.repositories.PositionRepository;
import com.employee.cuti.repositories.UserLeaveRequestRepository;
import com.employee.cuti.repositories.UsersRepository;

@RestController
@RequestMapping("/api")
public class UserRequestLeaveController {
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	
	@Autowired
	PositionRepository positionRepository;
	ModelMapper modelMapper = new ModelMapper();
	//mapping userLeaveRequest
	private List<UserLeaveRequestDTO> mappingUserLeaveRequest(){
		List<UserLeaveRequest> listUserLeaveRequest = userLeaveRequestRepository.findAll();
		List<UserLeaveRequestDTO> listUserLeaveRequestDTO = new ArrayList<UserLeaveRequestDTO>();
		for(UserLeaveRequest userLeaveRequest : listUserLeaveRequest) {
			UserLeaveRequestDTO userLeaveRequestDTO = modelMapper.map(userLeaveRequest, UserLeaveRequestDTO.class);
			listUserLeaveRequestDTO.add(userLeaveRequestDTO);
		}
		return listUserLeaveRequestDTO;
	}
	
	//mapping positionLeave
	private List<PositionLeaveDTO> mappingPositionLeave(){
		List<PositionLeave> listPositionLeave = positionLeaveRepository.findAll();
		List<PositionLeaveDTO> listPositionLeaveDTO = new ArrayList<PositionLeaveDTO>();
		for(PositionLeave positionLeave : listPositionLeave) {
			PositionLeaveDTO posisiLeaveDTO = modelMapper.map(positionLeave	, PositionLeaveDTO.class);
			listPositionLeaveDTO.add(posisiLeaveDTO);
		}
		return listPositionLeaveDTO;
	}
	
	//mapping position
	private List<PositionDTO> mappingPosisition(){
		List<Position> listPosition = positionRepository.findAll(Sort.by(Sort.Direction.ASC, "positionName"));
		List<PositionDTO> listPositionDTO = new ArrayList<PositionDTO>();
		for(Position position : listPosition) {
			PositionDTO positionDTO = modelMapper.map(position, PositionDTO.class);
			listPositionDTO.add(positionDTO);
		}
		return listPositionDTO;
	}
	//api requestleave
	@PostMapping("/requestLeave")
	public Map<String, Object> requestLeave(@Valid @RequestBody UserLeaveRequestDTO userLeaveRequestDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		//get date day off from and to
		long timeFrom = userLeaveRequestDTO.getLeaveDateFrom().getTime() /(24 * 60 * 60 * 1000);
		long timeTo = userLeaveRequestDTO.getLeaveDateTo().getTime() /(24 * 60 * 60 * 1000);
		//get selisih day
		long selisih = timeTo - timeFrom;
		String defaultStatus = "Waiting";
		Date today = new Date();
		long timeNow = today.getTime()/(24 * 60 * 60 * 1000);
		
		UserLeaveRequest userLeaveRequest = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
		long selisihToday = timeFrom - timeNow;
		//is exist
		boolean isExist = false;
		List<PositionLeaveDTO> listPositionLeaveDTO = mappingPositionLeave();
		List<UserLeaveRequestDTO> listUserLeaveRequestDTO = mappingUserLeaveRequest();
		for(UserLeaveRequestDTO userLeaveRequestDTOExist : listUserLeaveRequestDTO) {
			//check is data was exist, if exist remainingdaysoff will be set by previous data on the table with the same id and year
			if(userLeaveRequest.getUsers().getIdUser() == userLeaveRequestDTOExist.getUsers().getIdUser() && userLeaveRequest.getLeaveDateFrom().getYear() == userLeaveRequestDTOExist.getLeaveDateFrom().getYear() ) {
				
				isExist = true;
				userLeaveRequest.setRemainingDaysOff(userLeaveRequestDTOExist.getRemainingDaysOff());	
			}
		}
		if(!isExist) {
			//if data was not exist, remaining day off will be set by default number from table positionLeave
			Users users = usersRepository.findById(userLeaveRequest.getUsers().getIdUser()).get();
			for(PositionLeaveDTO positionLeaveDTO : listPositionLeaveDTO) {
				if(positionLeaveDTO.getPosition().getIdPosition() == users.getPosition().getIdPosition()) {
					userLeaveRequest.setRemainingDaysOff(positionLeaveDTO.getLeaveAllowance());
				}
			}
		}
		//if remaining day off = 0
		if(userLeaveRequest.getRemainingDaysOff()==0) {
			result.put("Error", "Jatah Cuti Habis");
			result.put("Message", "Mohon maaf jatah cuti anda habis");
		}
		//if selisih less than 0
		else if(selisih < 0) {
			result.put("Error", "Tanggal Salah, leaveDateForm > leaveDateTo");
			result.put("Message", "Tanggal yang anda ajukan tidak valid");
		}
		//if selisih more than remaining day off
		else if(selisih > userLeaveRequest.getRemainingDaysOff()){
			result.put("Error", "Jatah Cuti Tidak Cukup");
			result.put("Message", "Mohon maaf, jatah cuti anda tidak cukup digunakan untuk tanggal " + userLeaveRequest.getLeaveDateFrom() + " sampai " + userLeaveRequest.getLeaveDateTo() + "(" + selisih + " hari). Jatah cuti anda yang tersisa adalah " + userLeaveRequest.getRemainingDaysOff());
			
		}
		//if dayLeaveFrom < current date
		else if(selisihToday < 0) {
			result.put("Error", "Cuti backdate, tanggal pengajuan cuti < tanggal hari ini");
			result.put("Message", "Tanggal yang anda ajukan telah lampau , silahkan ganti tanggal pengajuan cuti anda.");
			
		}
		else {
			//message if success
			result.put("Success", 200);
			result.put("Message", "Permohonan anda sedang diproses");
			//set userLeaveRequest
			userLeaveRequest.setStatusPengajuan(defaultStatus);
			userLeaveRequest.setPengajuanDate(today);
			BucketApproval bucketApproval = new BucketApproval();
			bucketApproval.setUserLeaveRequest(userLeaveRequest);
			userLeaveRequestRepository.save(userLeaveRequest);
			UserLeaveRequest newUserLeaveRequest = userLeaveRequestRepository.findById(userLeaveRequest.getIdUserLeaveRequest()).get();
			bucketApproval.setUserLeaveRequest(newUserLeaveRequest);
			bucketApproval.setStatus(newUserLeaveRequest.getStatusPengajuan());
			bucketApproval.setIdBucketApproval(newUserLeaveRequest.getIdUserLeaveRequest());
			bucketApprovalRepository.save(bucketApproval);
		}
	
		return result;
	}
	//get data by user id and page
	@GetMapping("/listRequestLeave/{userId}/{totalDataPerPage}/{choosenPage}")
	public Map<String, Object> findListRequestLeave(@PathVariable(value = "userId") Long userId, @PathVariable(value = "totalDataPerPage")int totalDataPerPage, @PathVariable(value = "choosenPage")int choosenPage){
		//set choosen and total data per page
		Pageable pageable = PageRequest.of((choosenPage-1), totalDataPerPage);
		Page<BucketApproval> listBucketApproval = bucketApprovalRepository.findReqByUser(userId,pageable);
		List<BucketApprovalDTO> listBucketApprovalDTO = new ArrayList<BucketApprovalDTO>();
		//mapping bucketApproval
		for(BucketApproval bucketApproval : listBucketApproval) {
			BucketApprovalDTO bucketApprovalDTO = modelMapper.map(bucketApproval, BucketApprovalDTO.class);
			listBucketApprovalDTO.add(bucketApprovalDTO);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Response", "Success");
		result.put("Items", listBucketApprovalDTO);
		result.put("Total Item", listBucketApprovalDTO.size());
		return result;
	}
	
	@PostMapping("/resolveRequestLeave")
	public Map<String, Object> resolvingRequestLeave(@Valid @RequestBody BucketApprovalDTO bucketApprovalDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		List<UserLeaveRequestDTO> listUserLeaveRequest = mappingUserLeaveRequest();
		List<PositionDTO> listPosition = mappingPosisition();
		boolean isApproverRight = false;
		boolean isExist = false;
		String statusResolve = bucketApprovalDTO.getStatus();
		//set default value for deducting the remain day off by "Approved"
		String approval = "Approved";
		for(UserLeaveRequestDTO userLeaveRequestDTO : listUserLeaveRequest) {
			//validate id user
			if(userLeaveRequestDTO.getIdUserLeaveRequest() == bucketApprovalDTO.getIdBucketApproval()) {
				long leaveDateFrom = userLeaveRequestDTO.getLeaveDateFrom().getTime()/(24 * 60 * 60 * 1000); 
				long resolveDate = bucketApprovalDTO.getResolverDate().getTime() /(24 * 60 * 60 * 1000 + 1);
				long leaveDateTo = userLeaveRequestDTO.getLeaveDateTo().getTime()/(24 * 60 * 60 * 1000); 
				long rangeLeave = leaveDateTo - leaveDateFrom;
				long dateLeaveApprove = userLeaveRequestDTO.getPengajuanDate().getTime()/(24 * 60 * 60 * 1000);
				long remainsDayOff = userLeaveRequestDTO.getRemainingDaysOff();
				long rangeResolveFromDate = resolveDate - dateLeaveApprove;
				//validate if resolveDate < leaveDateForm
				if(rangeResolveFromDate < 0) {
					result.put("Error", "resolveDate < leaveDateForm");
					result.put("Message", "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti");
				}
				//if true
				else {
					System.out.println(bucketApprovalDTO.getUsers().getIdUser());
					Long idUser = bucketApprovalDTO.getUsers().getIdUser();
					Users user = usersRepository.findById(idUser).get();
					//checking the approver 
					//if approver corresponding with the rules "isApproveRight" will be true
					isApproverRight = checkingApprover(user, listPosition, userLeaveRequestDTO);
					if(isApproverRight) {
						//get selisih for sisa day off
						long leaveRest = remainsDayOff - rangeLeave;
						//validate if selisih < 0
						if (leaveRest < 0) {
							result.put("Response", "Error");
							result.put("Message", "Mohon maaf sisa cuti anda tidak cukup");
						}else {
							//set status and day off remain
							userLeaveRequestDTO.setStatusPengajuan(bucketApprovalDTO.getStatus());
							UserLeaveRequest userLeaveRequest = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
							BucketApproval bucketApproval = modelMapper.map(bucketApprovalDTO, BucketApproval.class);
							bucketApproval.setUserLeaveRequest(userLeaveRequest);
							bucketApprovalRepository.save(bucketApproval);
							//checking is status approved or not
							//if true it will be update the day off remain
							if(statusResolve.toLowerCase().indexOf(approval.toLowerCase()) != -1) {
								userLeaveRequest.setRemainingDaysOff((int) leaveRest);
								setRemainingDaysOff((int) leaveRest, userLeaveRequest.getLeaveDateFrom().getYear(), userLeaveRequest.getUsers().getIdUser());
							}
							userLeaveRequestRepository.save(userLeaveRequest);
							
							result.put("Response", "Success");
							result.put("Message", "Permohonan dengan ID " + bucketApprovalDTO.getIdBucketApproval() + " telah berhasil diputuskan");
						
						}
						}else {
						result.put("Response", "Error");
						result.put("Message", "Resolver tidak memiliki hak untuk approve cuti dari pegawai tersebut" );
					}
					
				}
				isExist = true;
			}
		}
		//if the id on the body doesnt exist at the table request
		if(!isExist) {
			result.put("Error", "idUserRequestLeave Tidak ada");
			result.put("Message", "Permohonan dengan ID " + bucketApprovalDTO.getIdBucketApproval() + " tidak ditemukan");
		}
		return result;
		
	}
	//checking approver for corresponding or not
	private boolean checkingApprover(Users user, List<PositionDTO> listPosition, UserLeaveRequestDTO userLeaveRequestDTO) {
		boolean isApproverRight = false;
		//the position name was sort by asc. so the parameter using the index of the list
		//if userRequestPosition = employee
		if(userLeaveRequestDTO.getUsers().getPosition().getPositionName() == listPosition.get(0).getPositionName()) {
			//check if the userApproverPosition = supervisor
			if(user.getPosition().getPositionName() == listPosition.get(2).getPositionName()) {
				isApproverRight = true;
			}
		}
		//if userRequestPosition == staff
		else if(userLeaveRequestDTO.getUsers().getPosition().getPositionName() == listPosition.get(1).getPositionName()) {
			//checking if the userApproverPosition = staff 
			if(user.getPosition().getPositionName() == listPosition.get(1).getPositionName()) {
				isApproverRight = true;
			}
		}
		//if userRequestPosition = supervisor
		else if(userLeaveRequestDTO.getUsers().getPosition().getPositionName() == listPosition.get(2).getPositionName()) {
			//check if userApproverPosition = supervisor and idUserReq != idUserApprover
			if(user.getPosition().getPositionName() == listPosition.get(2).getPositionName() && user.getIdUser() != userLeaveRequestDTO.getUsers().getIdUser()) {
				isApproverRight = true;
			}
		}
		return isApproverRight;
	}
	//set remaining day off with the same year and userId from userReq
	private void setRemainingDaysOff(int remainingDaysOff, int year, long idUser) {
		System.out.println("id" + idUser);
		List<UserLeaveRequestDTO> listUserRequestDTO = mappingUserLeaveRequest();
		for(UserLeaveRequestDTO userLeaveRequestDTO : listUserRequestDTO) {
			if(userLeaveRequestDTO.getLeaveDateFrom().getYear() == year && userLeaveRequestDTO.getUsers().getIdUser() == idUser) {
				UserLeaveRequest userLeaveRequest = modelMapper.map(userLeaveRequestDTO, UserLeaveRequest.class);
				userLeaveRequest.setRemainingDaysOff(remainingDaysOff);
				userLeaveRequestRepository.save(userLeaveRequest);
			}
		}
	}
	
	//api baru
	@GetMapping("/getAllDayOffRequest")
	public Map<String, Object> getAllDayOffReq(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<BucketApprovalDTO> listBucketApprovalDTO = mappingBucketApproval();
		result.put("Status", 200);
		result.put("Message", "Read All Data request day off succesful");
		result.put("Data", listBucketApprovalDTO);
		return result;
	}
	
	//mapping bucketApproval
	private List<BucketApprovalDTO> mappingBucketApproval() {
		List<BucketApproval> listBucketApproval = bucketApprovalRepository.findAll();
		List<BucketApprovalDTO> listBucketApprovalDTO = new ArrayList<BucketApprovalDTO>();
		for(BucketApproval bucketApproval : listBucketApproval) {
			BucketApprovalDTO bucketApprovalDTO = modelMapper.map(bucketApproval, BucketApprovalDTO.class);
			listBucketApprovalDTO.add(bucketApprovalDTO);
		}
		return listBucketApprovalDTO;
	}
}
