package com.employee.cuti.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.cuti.dtomodels.PositionLeaveDTO;
import com.employee.cuti.models.PositionLeave;
import com.employee.cuti.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("api/positionLeave")
public class PositionLeaveController {
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	ModelMapper modelMapper = new ModelMapper();
	
	@PostMapping("/create")
	public Map<String, Object> createPositionLeave(@Valid @RequestBody PositionLeaveDTO positionLeaveDTO ){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = modelMapper.map(positionLeaveDTO, PositionLeave.class);
		positionLeaveRepository.save(positionLeave);
		
		result.put("Status", 200);
		result.put("Messsage", "Input data positionLeave succesful");
		result.put("Data", positionLeave);
		return result;
		
	}
	
	@GetMapping("/readAll")
	public Map<String, Object> readAllData(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<PositionLeave> listPositionLeave = positionLeaveRepository.findAll();
		List<PositionLeaveDTO> listPositionLeaveDTO = new ArrayList<PositionLeaveDTO>();
		
		for(PositionLeave positionLeave : listPositionLeave) {
			PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
			listPositionLeaveDTO.add(positionLeaveDTO);
		}
		result.put("Status", 200);
		result.put("Messsage", "read all data positionLeave succesful");
		result.put("Data", listPositionLeaveDTO);
		return result;
	}
	
	@GetMapping("/read/{id}")
	public Map<String, Object> readDataById(@PathVariable(value = "id")Long idPositionLeave){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(idPositionLeave).get();
		PositionLeaveDTO positionLeaveDTO = modelMapper.map(positionLeave, PositionLeaveDTO.class);
		result.put("Status", 200);
		result.put("Messsage", "Read data positionLeave by id succesful");
		result.put("Data", positionLeaveDTO);
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateData(@PathVariable(value = "id") Long idPositionLeave, @Valid @RequestBody PositionLeaveDTO positionLeaveDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		PositionLeave positionLeave = positionLeaveRepository.findById(idPositionLeave).get();
		Date date = positionLeave.getCreatedDate();
		positionLeave = modelMapper.map(positionLeaveDTO, PositionLeave.class);
		positionLeave.setIdPositionLeave(idPositionLeave);
		positionLeave.setCreatedDate(date);
		positionLeaveRepository.save(positionLeave);
		result.put("Status", 200);
		result.put("Messsage", "update data positionLeave succesful");
		result.put("Data", positionLeave);
		return result;
	}
	
	@DeleteMapping("/delete/{id}")
	public Map<String, Object> deletePositionLeave(@PathVariable(value = "id") Long idPositionLeave){
		PositionLeave positionLeave = positionLeaveRepository.findById(idPositionLeave).get();
		positionLeaveRepository.delete(positionLeave);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("Status", 200);
		result.put("Messsage", "Delete data positionLeave succesful");
		result.put("Data", ResponseEntity.ok().build());
		return result;
	}
}
